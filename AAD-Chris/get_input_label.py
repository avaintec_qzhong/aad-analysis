'''
Created on 2018年3月20日

@author: ZhangMeiwei
'''
import numpy as np
import csv
import re



def read_label(labelPath,tag,numSamples):
    with open(labelPath,'r',encoding = 'utf-8') as f:
        label_data = f.readlines()
    label_array = np.zeros([numSamples,1])
    for i in range(len(label_data)):
        line = label_data[i].strip()
        if tag == 'liver':
            tmp1 = int(line)
            if tmp1 > 0:
                label_array[i][0] = 1
        elif tag == 'gallbladerr':
            tmp2 = int(line)
            if tmp2 > 0:
                label_array[i][0] = 1
        elif tag == 'pancrease':
            tmp3 = int(line)
            if tmp3 > 0:
                label_array[i][0] = 1
        elif tag == 'stomach':
            tmp4 = int(line)
            if tmp4 > 0:
                label_array[i][0] = 1
        elif tag == 'intestine':
            tmp5 = int(line)
            if tmp5 > 0:
                label_array[i][0] = 1
        elif tag == 'colon':
            tmp6 = int(line)
            if tmp6 > 0:
                label_array[i][0] = 1
        elif tag == 'appendix':
            tmp7 = int(line)
            if tmp7 > 0:
                label_array[i][0] = 1
        elif tag == 'peritoneum':
            tmp8 = int(line)
            if tmp8 > 0:
                label_array[i][0] = 1
        elif tag == 'inflammation':
            tmp9 = int(line)
            if tmp9 > 0:
                label_array[i][0] = 1
        elif tag == 'obstruction':
            tmp10 = int(line)
            if tmp10 > 0:
                label_array[i][0] = 1
        elif tag == 'hernia':
            tmp11 = int(line)
            if tmp11 > 0:
                label_array[i][0] = 1
        elif tag == 'Bleeding':
            tmp12 = int(line)
            if tmp12 > 0:
                label_array[i][0] = 1
        elif tag == 'perforation':
            tmp13 = int(line)
            if tmp13 > 0:
                label_array[i][0] = 1
        elif tag == 'cancer':
            tmp14 = int(line)
            if tmp14 > 0:
                label_array[i][0] = 1

    return label_array

if __name__ == '__main__':
    tag = 'liver'
    labelPath =  'data/' +tag + '/' + tag + '_test_label.csv'
    # labelPath = 'data/' + tag + '/' + tag + '_label_after_changeorder.csv'
    a,b,c,d,e,f ,g,h= read_label(labelPath,tag)
    # print(a)
    k = 0
    for i in e :
        if i[0] == 1:
            k +=1
    print(k)





