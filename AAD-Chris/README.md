# README #

### What is this repository for? ###


Chris开发的lstm+cnn模型的代码

### what are the folds for ###

data: 存放各个部位和肌肤症状的测试数据
-data.change_all_label_format.py: 将label转为自然数字0,1的方法
-data.change_order.py: 打乱样本顺序
-data.for_train_label.py: 数据重新打标签的程序，一个个的打

model：存放各个部位和肌肤症状的模型

stomach_test1：胃这个部位的测试脚本（应该是个人写的测试脚本，没有什么实际作用）

### The function of python footages ###

data_precess.py: 数据预处理方法

get_feature_input.py: 读取文本特征的方法

get_input_label.py: 读取label的方法

hy_config.py: 配置文件

model.py: 封装cnn+lstm模型的文件

predict.py: 预测方法

test_pkl.py: 读取二进制pkl文件

train.py: 训练方法，入口程序

utils.py: 工具方法


### How do I get set up? ###
模型训练：python train.py
模型预测：python test.py




