
from gensim.models import word2vec
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import jieba.posseg
import tensorflow as tf
import warnings
warnings.filterwarnings(action='ignore', category=UserWarning, module='gensim')
import gensim
import numpy as np
import re
import  csv
import get_input_label
import  collections
import pickle as pickle


samplefilename =  "./data/test_feat_22.csv"

def read_row_from_csv_test(filename,row_number):
    csvfile = open(filename,'r')
    result = csv.reader(csvfile)
    result_list= []
    row = []
    for line in result:
        result_list.append(line)
    for i in result_list:
        row.append(i[row_number-1])
    return row
if __name__ == "__main__":
    # row = read_row_from_csv_test(samplefilename,1)
    # print(len(row))
    csvfile = open(samplefilename,'r')
    result = csv.reader(csvfile)
    result_list = []
    row = []
    for line in result:
        result_list.append(line)
        print(line)

    print(len(result_list))