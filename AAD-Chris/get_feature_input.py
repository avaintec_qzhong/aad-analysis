'''
Created on 2018年4月16日

@author: ZhangMeiwei
'''
import numpy as np
import  csv
import get_input_label
import pickle as pickle


m_seq_length = 700

   
def read_from_csv(filename):
    csvfile = open(filename,'r',encoding='utf-8')
    result = csv.reader(csvfile)
    result_list= []
    for line in result:
        result_list.append(line)
    return result_list

def transform(vocab, d):
    d = list(d)
    new_d = []
    for word in d:
        if word in vocab.keys():
            new_d.append(vocab[word])
        else:
            new_d.append(0)
    # new_d = list(map(vocab.get, d[:m_seq_length]))
    if len(new_d) >= m_seq_length:
        new_d = new_d[:m_seq_length]
    else:
        new_d = new_d + [0] * (m_seq_length - len(new_d))
    return new_d

# if __name__ == '__main__':
#     with open('./data/covab.pkl', 'br') as f:
#         vocab = pickle.load(f)
#     vocab = vocab
#     print(vocab.get)
#     words = '代号为按开发和燃放？'
#     words = list(words)
#     for word in words:
#         if word in vocab.keys():
#             print(vocab[word])
#         else:
#             print(0)
#     a  = transform(vocab, words)
#     print(a)

#for training
def GetIds(samplefilename,labelPath,tag):
    numSamples = 2758
    with open('./data/covab.pkl', 'br') as f:
        vocab = pickle.load(f)
    vocab = vocab
    ids = np.zeros((numSamples, m_seq_length + 1))
    data = read_from_csv(samplefilename)
    label_array = get_input_label.read_label(labelPath, tag, numSamples)

    # if tag == 'liver':
    #     label_array = get_input_label.read_label(labelPath, tag, numSamples)
    # elif tag == 'gallbladerr':
    #     label_array = get_input_label.read_label(labelPath, tag, numSamples)
    # elif tag == 'pancrease':
    #     label_array = get_input_label.read_label(labelPath, tag, numSamples)
    # elif tag == 'stomach':
    #     label_array = get_input_label.read_label(labelPath, tag, numSamples)
    # elif tag == 'intestine':
    #     label_array = get_input_label.read_label(labelPath, tag, numSamples)
    # elif tag == 'colon':
    #     label_array = get_input_label.read_label(labelPath, tag, numSamples)
    # elif tag == 'appendix':
    #     label_array = get_input_label.read_label(labelPath, tag, numSamples)
    # elif tag == 'peritoneum':
    #     label_array = get_input_label.read_label(labelPath, tag, numSamples)
    numWords = []
    for i in range(len(data)):
        value = list(data[i][0] + data[i][1]+ data[i][2])
        words = []
        for word in value:
            words.append(word)
        counter = len(words)
        numWords.append(counter)
        # print(words)
        ids[i][:m_seq_length] = transform(vocab, words)
        ids[i][-1] = label_array[i]
    ids = ids.astype(int)
    # np.save('idsMatrix_'+ tag + '_train',ids)
    np.savetxt('./data/' + tag + '/' + tag + '_train.csv', ids, delimiter=',', fmt="%d")
    print('get ids file successful')

    return  numWords

import matplotlib.pyplot as plt
# if __name__ == "__main__":
#     tag_List11 = ['liver', 'gallbladerr', 'pancrease', 'stomach', 'intestine', 'colon', 'peritoneum',
#                 'appendix',]
#     tag_List = ['inflammation', 'obstruction', 'hernia', 'Bleeding', 'perforation', 'cancer']
#     for tag in tag_List:
#         samplefilename = 'data/' + tag + '/' + tag + '_train_data.csv'
#         labelPath = 'data/' + tag + '/' + tag + '_train_label_new.csv'
#         numwords = GetIds(samplefilename,labelPath,tag)

if __name__ == '__main__':
    tag_List = ['liver', 'gallbladerr', 'pancrease', 'stomach', 'intestine', 'colon', 'peritoneum',
                 'appendix','inflammation', 'obstruction', 'hernia', 'Bleeding', 'perforation', 'cancer']
    for tag in tag_List:
        labelPath =  'data/' +tag + '/' + tag + '_train.csv'
        # labelPath = 'data/' + tag + '/' + tag + '_label_after_changeorder.csv'
        # print(a)
        with open(labelPath) as f:
            label = f.readlines()
        k = 0
        print(len(label))
        for i in label :
            # print(i)
            i = i.strip()
            if i[-1] == '1':
                k +=1
        print(tag + ' negative sample number is :'+ str(k))



