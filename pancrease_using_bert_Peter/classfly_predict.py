#!/usr/bin/env python
# -*- coding: utf-8 -*-
class FLAGS:
    data_dir = 'data'
    # BERT预训练模型
    bert_config_file = 'config/bert_config.json'
    vocab_file='vocab/vocab.txt'
    output_dir = 'output/'
    init_checkpoint = 'chinese_L-12_H-768_A-12/bert_model.ckpt'
    #下游任务模型
    max_seq_length = 60
    max_predictions_per_seq = 20
    do_train = True
    do_eval = True
    do_predict = False
    do_lower_case = True
    train_batch_size = 8
    eval_batch_size = 8
    predict_batch_size = 8
    learning_rate = 5e-5
    num_train_epochs = 10
    warmup_proportion = 0.1
    dopredict = True
    num_train_steps = 100000
    num_warmup_steps = 10000
    save_checkpoints_steps = 1000
    iterations_per_loop = 1000
    max_eval_steps = 100
    
    # money is all you need  
    use_tpu = False
    tpu_name = None
    tpu_zone = None
    gcp_project = None
    master = None
    num_tpu_cores = 8
import modeling
import time
import tokenization
import tensorflow as tf
import os
import numpy as np
class InputExample(object):
    
    """A single training/test example for simple sequence classification."""

    def __init__(self, guid, text_a, text_b=None, label=None):
        self.guid = guid
        self.text_a = text_a
        self.text_b = text_b
        self.label = label

class InputFeatures(object):
    """A single set of features of data."""
    def __init__(self, input_ids, input_mask, segment_ids, label_id):
        self.input_ids = input_ids
        self.input_mask = input_mask
        self.segment_ids = segment_ids
        self.label_ids = label_id
def get_one_examples(text_a,text_b):
    """See base class."""
    return InputExample(guid="train-1", text_a=text_a,text_b=text_b, label='0')
def _truncate_seq_pair(tokens_a, tokens_b, max_length):
    """Truncates a sequence pair in place to the maximum length."""
    while True:
        total_length = len(tokens_a) + len(tokens_b)
        if total_length <= max_length:
            break
        if len(tokens_a) > len(tokens_b):
            tokens_a.pop()
        else:
            tokens_b.pop()


def convert_single_example(ex_index, example, label_list, max_seq_length,tokenizer):
    """Converts a single `InputExample` into a single `InputFeatures`."""
    label_map = {}
    for (i, label) in enumerate(label_list):
        label_map[label] = i
  
    tokens_a = tokenizer.tokenize(example.text_a)
    tokens_b = None
    if example.text_b:
        tokens_b = tokenizer.tokenize(example.text_b)
  
    if tokens_b:
    # Modifies `tokens_a` and `tokens_b` in place so that the total
    # length is less than the specified length.
    # Account for [CLS], [SEP], [SEP] with "- 3"
        _truncate_seq_pair(tokens_a, tokens_b, max_seq_length - 3)
    else:
      # Account for [CLS] and [SEP] with "- 2"
        if len(tokens_a) > max_seq_length - 2:
            tokens_a = tokens_a[0:(max_seq_length - 2)]
    tokens = []
    segment_ids = []
    tokens.append("[CLS]")
    segment_ids.append(0)
    for token in tokens_a:
        tokens.append(token)
        segment_ids.append(0)
    tokens.append("[SEP]")
    segment_ids.append(0)
  
    if tokens_b:
        for token in tokens_b:
            tokens.append(token)
            segment_ids.append(1)
        tokens.append("[SEP]")
        segment_ids.append(1)
  
    input_ids = tokenizer.convert_tokens_to_ids(tokens)

  # The mask has 1 for real tokens and 0 for padding tokens. Only real
  # tokens are attended to.
    input_mask = [1] * len(input_ids)

  # Zero-pad up to the sequence length.
    while len(input_ids) < max_seq_length:
        input_ids.append(0)
        input_mask.append(0)
        segment_ids.append(0)

    assert len(input_ids) == max_seq_length
    assert len(input_mask) == max_seq_length
    assert len(segment_ids) == max_seq_length

    label_id = label_map[example.label]
    if ex_index < 5:
        tf.logging.info("*** Example ***")
        tf.logging.info("guid: %s" % (example.guid))
        tf.logging.info("tokens: %s" % " ".join(
            [tokenization.printable_text(x) for x in tokens]))
        tf.logging.info("input_ids: %s" % " ".join([str(x) for x in input_ids]))
        tf.logging.info("input_mask: %s" % " ".join([str(x) for x in input_mask]))
        tf.logging.info("segment_ids: %s" % " ".join([str(x) for x in segment_ids]))
        tf.logging.info("label: %s (id = %d)" % (example.label, label_id))

    feature = InputFeatures(
        input_ids=input_ids,
        input_mask=input_mask,
        segment_ids=segment_ids,
        label_id=label_id)
    return feature


def create_model(bert_config, is_training, input_ids, input_mask, segment_ids,labels, num_labels, use_one_hot_embeddings):
    
    """Creates a classification model."""
    model = modeling.BertModel(
        config=bert_config,
        is_training=is_training,
        input_ids=input_ids,
        input_mask=input_mask,
        token_type_ids=segment_ids,
        use_one_hot_embeddings=use_one_hot_embeddings)

  # In the demo, we are doing a simple classification task on the entire
  # segment.
  #
  # If you want to use the token-level output, use model.get_sequence_output()
  # instead.
    output_layer = model.get_pooled_output()

    hidden_size = output_layer.shape[-1].value
  
    output_weights = tf.get_variable(
        "output_weights", [num_labels, hidden_size],
        initializer=tf.truncated_normal_initializer(stddev=0.02))

    output_bias = tf.get_variable(
        "output_bias", [num_labels], initializer=tf.zeros_initializer())

    with tf.variable_scope("loss"):
        if is_training:
        # I.e., 0.1 dropout
            output_layer = tf.nn.dropout(output_layer, keep_prob=0.9)

    logits = tf.matmul(output_layer, output_weights, transpose_b=True)
    logits = tf.nn.bias_add(logits, output_bias)
    probabilities = tf.nn.softmax(logits, axis=-1)
    log_probs = tf.nn.log_softmax(logits, axis=-1)

    one_hot_labels = tf.one_hot(labels, depth=num_labels, dtype=tf.float32)

    per_example_loss = -tf.reduce_sum(one_hot_labels * log_probs, axis=-1)
    loss = tf.reduce_mean(per_example_loss)

    return (loss, per_example_loss, logits, probabilities)

tf.reset_default_graph()  
is_training=False
use_one_hot_embeddings=False
batch_size=1

gpu_config = tf.ConfigProto()
gpu_config.gpu_options.allow_growth = True
sess=tf.Session(config=gpu_config)
model=None

global graph
input_ids_p,input_mask_p,label_ids_p,segment_ids_p=None,None,None,None

if not os.path.exists(os.path.join(FLAGS.output_dir, "checkpoint")):
    raise Exception("failed to get checkpoint. going to return ")
label_list = ['0','1']
num_labels = len(label_list) 

graph = tf.get_default_graph()
with graph.as_default():
    print("going to restore checkpoint")
    #sess.run(tf.global_variables_initializer())
    input_ids_p = tf.placeholder(tf.int32, [batch_size, FLAGS.max_seq_length], name="input_ids")
    input_mask_p = tf.placeholder(tf.int32, [batch_size, FLAGS.max_seq_length], name="input_mask")
    label_ids_p = tf.placeholder(tf.int32, [batch_size, 1], name="label_ids")
    segment_ids_p = tf.placeholder(tf.int32, [batch_size, FLAGS.max_seq_length], name="segment_ids")
    
    bert_config = modeling.BertConfig.from_json_file(FLAGS.bert_config_file)
    (total_loss, logits, trans, pred_ids) = create_model(bert_config, is_training, input_ids_p, input_mask_p, segment_ids_p,label_ids_p, num_labels, use_one_hot_embeddings)
    saver = tf.train.Saver()
    saver.restore(sess, tf.train.latest_checkpoint(FLAGS.output_dir))
    
tokenizer = tokenization.FullTokenizer(vocab_file=FLAGS.vocab_file, do_lower_case=FLAGS.do_lower_case)

def predict(sentence):
    """
    do online prediction. each time make prediction for one instance.
    you can change to a batch if you want.

    :param line: a list. element is: [dummy_label,text_a,text_b]
    :return:
    """
    def convert(sentence):
        feature = convert_single_example(0,get_one_examples(sentence,None), label_list, FLAGS.max_seq_length,tokenizer)
        
        input_ids = np.reshape([feature.input_ids],(batch_size, FLAGS.max_seq_length))
        input_mask = np.reshape([feature.input_mask],(batch_size, FLAGS.max_seq_length))
        segment_ids = np.reshape([feature.segment_ids],(batch_size, FLAGS.max_seq_length))
        label_ids = np.reshape([feature.label_ids],(batch_size, 1))
        return input_ids, input_mask, segment_ids, label_ids

    global graph
    with graph.as_default():
        start = time.clock()
        input_ids, input_mask, segment_ids, label_ids = convert(sentence)

        feed_dict = {input_ids_p: input_ids,
                     input_mask_p: input_mask,
                     segment_ids_p:segment_ids,
                     label_ids_p:label_ids}
        # run session get current feed_dict result
        pred_ids_result = sess.run([pred_ids], feed_dict)
        spend = time.clock()-start
        #print('spend time : {}'.format(spend))
    return pred_ids_result[0].tolist()

import matplotlib.pyplot as plt
import itertools
def plot_confusion_matrix(cm, classes, title='Confusion matrix'):
    plt.imshow(cm, interpolation='nearest', cmap=None)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=0)
    plt.yticks(tick_marks, classes)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

def plot_matrix(y_true, y_pred):
    from sklearn.metrics import confusion_matrix
    confusion_matrix = confusion_matrix(y_true, y_pred)
    class_names = ['positive', 'negative']
    plot_confusion_matrix(confusion_matrix
                          , classes=class_names
                          , title='Confusion matrix')

def sentence_predict(sentence):
    res= predict(sentence)
    return res[0].index(max(res[0]))

def batch_predict(file_path):
    test_list=[line.strip().split('\t') for line in open(file_path,"r",encoding='utf-8')]
    predict_res,true_label,if_correct=[],[],[]
    for each in test_list:
        res=predict(each[0])
        res_index=res[0].index(max(res[0]))
        predict_res.append(res_index)
        true_label.append(int(each[1]))
        #print(res)
        if res_index==int(each[1]):
            if_correct.append(1)
        else:
            if_correct.append(0)
    #compute accuracy
    total=0
    for ele in if_correct: 
        total = total + ele
    accuracy=total/len(if_correct)
    
    return predict_res,true_label,accuracy

#print(sentence_predict('下腹痛4+小时,否认高血压。糖尿病等慢性病史否认肝炎结核等传染病史3年前行腹腔镜下子宫肌瘤次全切除术诉青霉素过敏史否认输血史预防接种史不详,4+月前患者诉左上腹胀气无疼痛。呕吐。反酸等不适于中医院治疗未行影像学检查服中药后未见明显缓解4小时前无明显诱因突然出现右下腹疼痛痛为持续性胀痛伴恶心呕吐无反酸。嗳气遂来我院就诊急诊血常规未见明显异常；CT见:腹腔多发游离积气急诊以空腔脏器穿孔收入我科患病以来患者精神可饮食稍减大。小便无明显异常体重减轻5+kg'))
res,label,acc=batch_predict('data/test.tsv')

from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score

accuracy=accuracy_score(label, res)
precision=precision_score(label, res)
recall=recall_score(label, res)
f1=f1_score(label, res)

print('accuracy=%f'%accuracy)
print('precision=%f'%precision)
print('recall=%f'%recall)
print('f1=%f'%f1)



