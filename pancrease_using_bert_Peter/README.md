# README #

### What is this repository for? ###

Peter开发的基于BERT进行finetune的分类模型，整个project都基于bert的源码，通过修改processor（对应文本分类的processor，本项目增加了AADClassifierProcessor，详见run_classifier.py），用自己的数据进行finetune

### what are the folds for ###

config: 存放bert_config的目录

data：存放数据的目录

output：存放输出的目录

vocab：存放bert字典的目录

### The function of python footages ###


关键程序：（其他都是bert项目带的，不需要做调整的）
classfly_predict.py: 模型预测与评价的程序

pre_process.py: 模型预处理方法

run_classifier.py: finetune文本分类模型的程序


### How do I get set up? ###
模型训练：python run_classifier.py
模型预测：python classfly_predict.py




