#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pickle as pickle
import tensorflow as tf

from utils import TextLoader,TextLoader_test
from model import Model
import matplotlib.pyplot as plt
import itertools
num_epochs = 30
learning_rate = 0.001
decay_rate = 0.9
batch_size = 64
import  numpy as np

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES'] = "0"

def plot_confusion_matrix(cm, classes, title='Confusion matrix'):
    plt.imshow(cm, interpolation='nearest', cmap=None)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=0)
    plt.yticks(tick_marks, classes)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.show()

def plot_matrix(y_true, y_pred):
    from sklearn.metrics import confusion_matrix
    confusion_matrix = confusion_matrix(y_true, y_pred)
    class_names = ['positive', 'negative']
    plot_confusion_matrix(confusion_matrix
                          , classes=class_names
                          , title='Confusion matrix')



def read_dictionary(vocab_path):
    vocab_path = os.path.join(vocab_path)
    with open(vocab_path, 'rb') as fr:
        word2id = pickle.load(fr)
    id2word={v: k for k, v in word2id.items()}
    print('vocab_size:', len(word2id))
    print('vocab_size:', len(id2word))
    return word2id,id2word

def random_embedding(vocab, embedding_dim):
    embedding_mat = np.random.uniform(-0.25, 0.25, (len(vocab)+1, embedding_dim))
    # embedding_mat = np.float32(embedding_mat)
    return embedding_mat


def train(embeddings,Ture,tag):
    data_loader = TextLoader(batch_size,tag)
    vocab_size = data_loader.vocab_size
    print('vocab_size',vocab_size)
    print('downloading model.......')
    model = Model(embeddings,Ture)
    print('finished downloading model.......')
    best_accuracy=0
    lowest_loss=1.0    
    tf_config = tf.ConfigProto()
    tf_config.gpu_options.allow_growth = True    
    saver = tf.train.Saver()
    with tf.Session(config=tf_config) as sess:
        sess.run(tf.global_variables_initializer())
        writer = tf.summary.FileWriter('./tf_log/' + tag + '/', sess.graph)
        print('start loop .......')
        for e in range(num_epochs):
            sess.run(tf.assign(model.lr, learning_rate * (decay_rate ** e)))
            data_loader.shuff()
            for b in range(data_loader.num_batches):
                x, y = data_loader.next_batch(b)
                feed = {model.input_data: x, model.targets: y}
                predict_prob,train_loss, state, _, accuracy,summary_,output3 = sess.run([model.probs,model.cost, model.final_state, model.optimizer, model.accuracy,model.merged,model.output3], feed_dict=feed)
                writer.add_summary(summary_, global_step=b)
                print('第{}批次，第{}次循环时,train_loss ={},accuracy ={},process tag is {}'.format(e,b,train_loss,accuracy,tag))

                #if e == num_epochs-1 and b == data_loader.num_batches-1:
                if accuracy>best_accuracy and train_loss<lowest_loss:
                    best_accuracy=accuracy
                    lowest_loss=train_loss
                    saver.save(sess,'./new_test_final/'+ tag + '/' + tag + '.model')
                    print('model saved at accuracy:%f,loss:%f'%(best_accuracy,lowest_loss))
                if e%7==0:
                    saver.save(sess,'./new_test_final/'+ tag + '/' + tag + '.model')
                    print('model saved after 7th epoch at accuracy:%f,loss:%f'%(best_accuracy,lowest_loss))
                




if __name__ == '__main__':

    tag_List = ['stomach']
    for tag in tag_List:
        data_loader = TextLoader_test(64,tag)
        # saver = tf.train.import_meta_graph('new_test_final/' + tag + '/' + tag + '.model.meta')
        saver = tf.train.import_meta_graph('new_test_final/' + tag + '/' + tag + '.model.meta')
        with tf.Session() as sess:
            sess.run(tf.global_variables_initializer())
            saver.restore(sess, tf.train.latest_checkpoint('new_test_final/' + tag + '/'))
            inputs = tf.get_default_graph().get_tensor_by_name('inputs:0')
            y = tf.get_default_graph().get_tensor_by_name('softmaxLayer/y:0')
            true_label = []
            pre_label = []
            for i in range(data_loader.num_batches):
                x, label = data_loader.next_batch(i)
                prediction = sess.run(y,{inputs:x})
                prediction = np.argmax(prediction,1)
                for i in label:
                    true_label.append(i)
                for j in prediction:
                    pre_label.append(j)

        plot_matrix(true_label, pre_label)

    #data_loader = TextLoader(4,'stomach')
    #word2id,id2word = read_dictionary('./data/covab.pkl')
    #embeddings = random_embedding(word2id, 100)
    #embeddings=data_loader.load_word2vec('./vectors/medical_vec_character.txt',id2word, 100, embeddings)
    #tag_List = ['stomach']

    #for tag in tag_List:
        #train(embeddings, True,tag)
    # tag_List = ['pancrease','intestine','colon','appendix','inflammation','obstruction','perforation', 'hernia']
    # 'liver', 'gallbladerr', 'pancrease', 'stomach', 'intestine', 'colon',
    # 'appendix', 'peritoneum', 'inflammation', 'Bleeding', 'obstruction',
    # 'perforation', 'hernia', 'cancer']
    #


