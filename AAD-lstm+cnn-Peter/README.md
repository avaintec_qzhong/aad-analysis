# README #

### What is this repository for? ###


Peter开发的lstm+cnn模型的代码

### what are the folds for ###

data: 存放各个部位和肌肤症状的测试数据

new_test_final：存放各个部位和肌肤症状的模型

tf_log：存放tf训练日志

vectors：存放预训练词向量

### The function of python footages ###

model.py: 封装cnn+lstm模型的文件

train.py: 训练方法，入口程序

utils.py: 工具方法


### How do I get set up? ###
模型训练：python train.py




