#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pickle as pickle
import numpy as np
import pandas as pd
import re
import codecs
from sklearn.model_selection import train_test_split

class TextLoader(object):
    def __init__(self, batch_size,tag = None):
        self.tag = tag
        self.data_path = './new_test_final/' + self.tag +  '_train.csv'
        self.batch_size = batch_size
        self.seq_length = 700
        self.encoding = 'utf8'
        with open( './data/covab.pkl', 'br') as f:
            vocab = pickle.load(f)
        self.vocab = vocab
        self.vocab_size = len(vocab) + 1
        self.load_preprocessed(self.data_path)
        self.shuff()

    def load_preprocessed(self,data_path):
        if self.tag == 'inflammation111':
            data = pd.read_csv(data_path)
            number_sick = len(data[data.label == 0])
            sick_indices = np.array(data[data.label == 0].index)

            normal_indices = data[data.label == 1].index
            random_normal_indices = np.random.choice(normal_indices, number_sick, replace=False)
            random_normal_indices = np.array(random_normal_indices)
            under_sample_indices = np.concatenate([sick_indices, random_normal_indices])
            under_sample_data = data.iloc[under_sample_indices, :]
            X_undersample = under_sample_data.ix[:, under_sample_data.columns != 'label']
            y_undersample = under_sample_data.ix[:, under_sample_data.columns == 'label']
            tensor_x = np.array(X_undersample)
            tensor_y = np.array(y_undersample)
            self.tensor = np.c_[tensor_x, tensor_y].astype(int)
        else:
            data = pd.read_csv(data_path)
            X = data.ix[:, data.columns != 'label']
            Y = data.ix[:, data.columns == 'label']
            number_sick = len(data[data.label == 1])
            sick_indices = np.array(data[data.label == 1].index)
            normal_indices = data[data.label == 0].index
            random_normal_indices = np.random.choice(normal_indices, number_sick, replace=False)

            random_normal_indices = np.array(random_normal_indices)
            under_sample_indices = np.concatenate([sick_indices, random_normal_indices])

            under_sample_data = data.loc[under_sample_indices, :]

            X_undersample = under_sample_data.ix[:, under_sample_data.columns != 'label']
            y_undersample = under_sample_data.ix[:, under_sample_data.columns == 'label']
            # tensor_x = np.array(X_undersample)
            # tensor_y = np.array(y_undersample)
            tensor_x = np.array(X)
            tensor_y = np.array(Y)
            self.tensor = np.c_[tensor_x, tensor_y].astype(int)

    def shuff(self):
        self.num_batches = int(self.tensor.shape[0] // self.batch_size)
        if self.num_batches == 0:
            assert False, 'Not enough data, make batch_size small.'
        np.random.shuffle(self.tensor)

    def next_batch(self,k):
        x = []
        y = []
        for i in range(self.batch_size):
            tmp = np.array(list(self.tensor)[k*self.batch_size + i][:self.seq_length])
            x.append(tmp)
            tmp2 = np.array(list(self.tensor)[k*self.batch_size + i][-1])
            y.append(tmp2)
        return np.array(x), np.array(y)
    
    
    def load_word2vec(self,emb_path, id_to_word, word_dim, old_weights):
        """
        Load word embedding from pre-trained file
        embedding size must match
        """
        new_weights = old_weights
        print('Loading pretrained embeddings from {}...'.format(emb_path))
        pre_trained = {}
        emb_invalid = 0
        for i, line in enumerate(codecs.open(emb_path, 'r', 'utf-8')):
            line = line.rstrip().split()
            if len(line) == word_dim + 1:
                pre_trained[line[0]] = np.array(
                    [float(x) for x in line[1:]]
                ).astype(np.float32)
            else:
                emb_invalid += 1
        if emb_invalid > 0:
            print('WARNING: %i invalid lines' % emb_invalid)
        c_found = 0
        c_lower = 0
        c_zeros = 0
        n_words = len(id_to_word)
        # Lookup table initialization
        for i in range(n_words):
            word = id_to_word[i]
            if word in pre_trained:
                new_weights[i] = pre_trained[word]
                c_found += 1
            elif word.lower() in pre_trained:
                new_weights[i] = pre_trained[word.lower()]
                c_lower += 1
            elif re.sub('\d', '0', word.lower()) in pre_trained:
                new_weights[i] = pre_trained[
                    re.sub('\d', '0', word.lower())
                ]
                c_zeros += 1
        print('Loaded %i pretrained embeddings.' % len(pre_trained))
        print('%i / %i (%.4f%%) words have been initialized with '
              'pretrained embeddings.' % (
            c_found + c_lower + c_zeros, n_words,
            100. * (c_found + c_lower + c_zeros) / n_words)
        )
        print('%i found directly, %i after lowercasing, '
              '%i after lowercasing + zero.' % (
            c_found, c_lower, c_zeros
        ))
        return new_weights    

class TextLoader_test(object):
    def __init__(self,batch_size,tag):
        self.tag = tag
        self.data_path = './new_test_final/' +  self.tag + '_test.csv'
        self.batch_size = batch_size
        self.seq_length = 700
        self.encoding = 'utf8'
        with open('./data/covab.pkl', 'br') as f:
            vocab = pickle.load(f)
        self.vocab = vocab
        self.vocab_size = len(vocab) + 1
        self.load_preprocessed(self.data_path)
        self.shuff()
    def load_preprocessed(self,data_path):
        data = pd.read_csv(data_path)
        X = data.ix[:, data.columns != 'label']
        Y = data.ix[:, data.columns == 'label']
        tensor_x = np.array(X)
        tensor_y = np.array(Y)
        self.tensor = np.c_[tensor_x, tensor_y].astype(int)
    def shuff(self):
        self.num_batches = int(self.tensor.shape[0] // self.batch_size)
        np.random.shuffle(self.tensor)
    def next_batch(self,k):
        x = []
        y = []
        for i in range(self.batch_size):
            tmp = np.array(list(self.tensor)[k*self.batch_size + i][:self.seq_length])
            x.append(tmp)
            tmp2 = np.array(list(self.tensor)[k*self.batch_size + i][-1])
            y.append(tmp2)
        return np.array(x), np.array(y)









if __name__ == "__main__":
    data_loader = TextLoader_test(2,'liver')
    print(data_loader.tensor.shape)
    k = 0
    for i in data_loader.tensor:
        if i[700] == 1:
            k+=1
    print(k)
    x,y = data_loader.next_batch(4)
    print(y)
    # print(data_loader.tensor.shape)
    # a,b = data_loader.next_batch(3)
    # print(a.shape)
    # print(a)
    # print(b)
    # c,d = data_loader.next_batch(1)
    # print(c.shape)
    # print(d)



